# 2 Task

The algorithm, assuming that each process knows at most the id/locator/name of another process, must ensure that when the group is formed, all its components have the id/locator/name of the other members.

We have seen two possibilities:

+ with server

+ without server

Choose one of them:

Algorithm design: 1. diagram to say which methods each process has + n diagrams to detail the interactions.

2. Design of the zeroMQ implementation connections.

Note: you can deliver the above in a single png/jpg/... file made with draw.io or similar.

3. Implementation in node.js+zeroMQ: the "semantic" functions expressed in step 1 of the communication part must be separated.