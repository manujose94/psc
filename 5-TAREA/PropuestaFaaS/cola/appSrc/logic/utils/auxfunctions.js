//funcions auxiliars
 	
// *** getLoad function
var ordered = false;

function checkMessageRegistry(msg){
	let val;
	if(msg instanceof Object)
		val=msg
	else val=checkFormat(msg)
	console.log(val.name_function)
	if(val && val.name_function && val.code_function)
		return val
	return false	
}
function checkMessageLaunch(msg){
	let val;
	if(msg instanceof Object)
		val=msg
	else val=checkFormat(msg)
	if(val && val.name_function)
		return val
	return false	
}
function checkFormat(text){
		if (typeof text!=="string"){
			return false;
		}
		try{
			let obj=JSON.parse(text);
			return obj;
		}
		catch (error){
			return false;
		}	
}

 	function getLoad() {
 	  var fs     = require('fs')
 	    , data   = fs.readFileSync("/proc/loadavg") // version sincrona 
 	    , tokens = data.toString().split(' ') 
 	    , min1   = parseFloat(tokens[0])+0.01
 	    , min5   = parseFloat(tokens[1])+0.01
 	    , min15  = parseFloat(tokens[2])+0.01
 	    , m      = min1*10 + min5*2 + min15;
 	  return m; 
 	}
 	
 	  // *** randNumber function
 	function randNumber(upper, extra) {
 	  var num = Math.abs(Math.round(Math.random() * upper));
 	  return num + (extra || 0);
 	}
 	
 	  // *** randTime function
 	function randTime(n) {
 	  return Math.abs(Math.round(Math.random() * n)) + 1;
 	}
 	
 	  // *** showArguments function
 	function showMessage(msg) {
 	  msg.forEach( (value,index) => {
 	    console.log( '     Segment %d: %s', index, value );
 	  })
 	}
 	
 	  // *** ordered list functions for workers management
 	  // list has an "ordered" property (true|false) and a data property (array)
 	  // data elements consists of pairs {id, load}
 	function orderedList() {
 	  return {ordered: true, data: []};
 	}	
 	function nonempty() {
 	  return (this.data.length > 0);
 	}
	function lowest() {  // ordered reads
		if (!this.ordered) {
			data.sort(function (a, b) {
				return parseFloat(b.load) - parseFloat(a.load);
			})
		}
		this.ordered = true;
		return this.data.shift();
	}
 	
 	function insert(k) { // unordered writes
 	  this.ordered = false;
 	  this.data.push(k);
      }

function randomIntInc(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low);
      }

function lowestPesos(data) {  // ordered reads
    if (!this.ordered) {
        data.sort(function (a, b) {
            return parseFloat(a.load) - parseFloat(b.load);
        });
    }
    this.ordered = true;
    //console.log("this.ordered: " + this.ordered)
    return data.shift(); //{ m: 0 }
}

function show(data) {  // ordered reads
  
    console.log("\n");
    console.log("*******Workers Available*******");
    for (var myobject in data) {
        console.log( + data[myobject].load + ": " + data[myobject].worker);
    }
    console.log("************************\n");
    
}

function insertWorkerLoad(k,data) { // unordered writes
    this.ordered = false;
    data.push(k);
}
function deleteWorkerLoad(id,data) { // unordered writes

    for (let [i, w] of data.entries()) {
		if (w.worker == id) {
			data.splice(i, 1);
		}
	 }
}

function existWorkerLoad(id,data) { // unordered writes
	let find=false
	for (var myobject in data) {
		if (data[myobject].worker == id){
			find= true 
		break
		}
	}
	 return find;
}
module.exports.existWorkerLoad=existWorkerLoad;
module.exports.deleteWorkerLoad = deleteWorkerLoad;
module.exports.checkMessageRegistry = checkMessageRegistry;
module.exports.checkMessageLaunch = checkMessageLaunch;
module.exports.getLoad = getLoad;
module.exports.randNumber = randNumber;
module.exports.randTime = randTime;
module.exports.showMessage = showMessage;
module.exports.orderedList = orderedList;
module.exports.nonempty = nonempty;
module.exports.lowest = lowest;
module.exports.insert = insert; 
module.exports.randomIntInc = randomIntInc; 
module.exports.lowestPesos = lowestPesos;
module.exports.show = show;
module.exports.insertWorkerLoad = insertWorkerLoad;