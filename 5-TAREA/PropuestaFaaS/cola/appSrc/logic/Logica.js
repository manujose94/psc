// ---------------------------------------------------------------------
// Logica.js 
// ---------------------------------------------------------------------
var Queue = require('./Queue.js');
var aux = require("./utils/auxfunctions.js");
class Logica {
	// -----------------------------------------------------------------
	//    constructor ()  -->
	// -----------------------------------------------------------------
	constructor( reject, resolve ) {
		this.miColaPendiente= new Queue();
		this.miColaResultado= [];
		this.miHistoryResults=[];
		this.lasFunciones=[];
		this.waitResultJob=[]
		console.log( "[C.L] Logica.constructor()")
		resolve( this ) 
	}

	prueba( ) {
		return "hola desde la logica del BO COLA\n" 
	}
	//Add to the list of functions or jobjs
	registrarTrabajo(mfunc,JobID) {
		let resgitry_json=aux.checkMessageRegistry(mfunc)
		
		if(!resgitry_json) return false
		
		this.lasFunciones[resgitry_json.name_function]=resgitry_json;
		
		return resgitry_json;
	}

	encargarTrabajo(jobid,tareaJSON,clientID) {
		let job=aux.checkMessageLaunch(tareaJSON)
		if(job){
			if(this.existFunc(job.name_function) || job.code_function){ //Permitir sin registro lanzar funcion si hay código
				
				if(!this.lasFunciones[job.name_function]){
					let resgistry=this.registrarTrabajo(mfunc)
					if(resgitry)console.log("[C] Logic: Task added and resgistred at the same time")
				}
					
				this.miColaPendiente.enqueue([this.lasFunciones[job.name_function],clientID,jobid.toString()])
				
				console.log(this.miColaPendiente)
				return jobid
			}else{
				return null
			}
		}else return false
		
	}

	existFunc(name_function){
		if(this.lasFunciones[name_function]) return true
		else false
	}

	//Get first pending order available
	getEncargo(){
		let encargo=this.miColaPendiente.dequeue();	
		return encargo 
	}

	setSolEncargo(jobid,result){
			this.miColaResultado[jobid]=result
			this.miHistoryResults.push({jobid:jobid,result:result})
			console.log(this.miColaResultado)
	}

	setClientWaiting(ClientId,jobid){
		this.waitResultJob[jobid]={clientid: ClientId}
		
	}

	existClientWaitJob(jobid){
		jobid=jobid.toString()
		console.log({jobid},this.waitResultJob[jobid])
		if(this.waitResultJob[jobid]){
			let solEncargo= this.getSolEncargo(jobid);
			console.log({solEncargo})
			if(solEncargo)
				return [this.waitResultJob[jobid].clientid,'',solEncargo,jobid]
		}
		
		return false


	}
	getSolEncargo(jobid){
		console.log("[L.C] miColaResultado: ",this.miColaResultado)
		
		let index=null;
		if(this.miColaResultado[jobid]){
			let jobSol=this.miColaResultado[jobid]
			delete this.miColaResultado[index]
			return jobSol
		}else return false
	}

	getFuncionesRegistradas(){
		return this.lasFunciones
	}
	getHistoryResults(){
		return this.miHistoryResults
	}
							  
} // class

module.exports.nueva = function ( ) {
	return new Promise( (resolve, reject) => {
		// OJO: si new Logica va bien, devuelve un ptr a sí
		// mismo en el segundo callback
		new Logica( 
			( err ) => {
				console.log( "[CO] Logica.nueva() reject(): " + err )
				reject( err )
			},
			( logica ) => {
				console.log( "[CO] Logica.nueva() resolve(): ", {logica})
				resolve( logica ) 
			})
	} )
} // ()
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
