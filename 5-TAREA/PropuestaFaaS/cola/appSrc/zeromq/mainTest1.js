
// ---------------------------------------------------------------
// ---------------------------------------------------------------
var zmq = require ('zeromq')

var elSocket = zmq.socket ('req')

// ---------------------------------------------------------------
// ---------------------------------------------------------------
var peticiones = []

peticiones.siguiente = function () {
	var peti = this.shift ()
	if ( ! peti ) return
	elSocket.send ( peti ) 
}

peticiones.push ( ["PRUEBA", '{"key": "axxx", "value":"1234"}'])

// ---------------------------------------------------------------
// ---------------------------------------------------------------
elSocket.on("message", function() {
	var args = Array.from (arguments)

	var respuesta = args[0]
	// var error = args[1]  

	console.log (" ** respuesta recibida ")
	console.log (" respuesta: " + respuesta )

	peticiones.siguiente ()

})

// ---------------------------------------------------------------
// ---------------------------------------------------------------
process.on('SIGINT', function() {
  elSocket.close();
})

// ---------------------------------------------------------------
// ---------------------------------------------------------------
// conectar
elSocket.connect("tcp://localhost:6001")

// elSocket.send( ["uno", "dos", "tres", "cuatro"] )

// ---------------------------------------------------------------
// ---------------------------------------------------------------
peticiones.siguiente ()

