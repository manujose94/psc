
var zmq = require("zeromq")
const Logica = require("../logic/Logica.js")
const EventEmitter = require("events");
class Dispatcher extends EventEmitter {

	// -----------------------------------------------------------------
	// 1.CallError are for tail logic errors
	// For example: That the message received is not suitable or not registered
	//
	// 2. Launch funtcion dmust be registered, if there is code_function in the package
	// Allows to launch directly without registering (OPTIONAL)
	// -----------------------------------------------------------------
	constructor(logicCola, tipoSocket, urlSocket, urlSocketTreballs) {
		super();
		this.lasFunciones = logicCola.lasFunciones
		this.elSocket = zmq.socket(tipoSocket)
		this.elSocketTreballs = zmq.socket('xrep');
		this.elSocketTreballs.identity='xrep_' + process.pid;
		this.pendentTask=[]
		this.clientID = "IDunicClient";
		this.logicCola = logicCola;
		var self = this

		
		this.elSocket.on("message", function () {
			try {
			// Array of elements: [TYPEREQUEST,MESSAGE:JSON,jobid,]
			// First element: Type  request: REGISTRY,LAUNCH
			// Second element:  Json with {name_function: , code_function: }
			var args = Array.apply(null, arguments);
			var jobid = args.pop();
			var peticio = args[0].toString()
			var mfunc = args[1].toString()

			console.log(`[C] servidor: Arriba: ${peticio} : ${mfunc}`)

			self.checkMessagefromClient(self, peticio, mfunc, jobid)

			 } catch( err ) {
			   // some error, then send 500
			   self.elSocket.send([
				   JSON.stringify( {
					   callError: 500,
					   message: "Server Error: " + err
				   }) , jobid]
			   )
		   }
		})

		this.elSocketTreballs.on("message", function () {
			try {
			// Array of elements: [clientid,'',jobid, "WAIT_TASK", 5000]
			let args = Array.apply(null, arguments);
			var clientID = args[0].toString();
			args=args.slice(2)// deleted [clientid,'']
			var jobid=args[0].toString()
			let status = args[1].toString()
			let wait_time=6000
			if(args[2]) //Timout without use (create setTimout that send call Error Timeout)
				wait_time = parseInt(args[2].toString())

			console.log(`[C Sol] servidor Resultat[${jobid}]: status: ${status} : ${wait_time}`)

			self.checkMessageClientWait(self, clientID,status,wait_time, jobid)

			 } catch( err ) {
			   // some error, then send 500 to cliente via identity
			   self.elSocketTreballs.send([clientID,"",
				   JSON.stringify( {
					   callError: 500,
					   message: "Server Error: " + err
				   }),jobid]
				)
		   }
		})

	//BIND TWO DIFFERENT SOCKETS
		//Bind Socket rep (Listen for Registry or Luanching)
		self.elSocket.bind(urlSocket, (err) => {
			if (err) {
				console.log("Error en bind: " + err)
			} else {
				console.log("[C] Listen Frontend on" + urlSocket)
			}
		})
		/**
		 *  Bind Socket XRep
		 *  Remove the necessity for the lock step send/recv
		 * 	But it's necessary a identity on sokcet req.
		 * 	XREP will add the identifier on the front of the message when sending
		 */
		this.elSocketTreballs.bind(urlSocketTreballs, (err) => {
			if (err) {
				console.log("Error en bind: " + err+" - "+urlSocketTreballs)
			} else {
				console.log("[C] Listen for SolTreballs on " + urlSocketTreballs)
			}
		})
	}
	checkMessageClientWait(_self, clientid,peticio, time, jobid){
		switch (peticio) {
			//If there ir sol: [clientID,"",msg:JSON:string,jobid]
			case "WAIT_TASK":
				let SolEncargo = _self.logicCola.getSolEncargo(jobid)
				console.log("WAIT_TASK with sol",{ SolEncargo })
				if(SolEncargo){
					_self.elSocketTreballs.send([clientid,"",SolEncargo,jobid])
				}else _self.logicCola.setClientWaiting(clientid,jobid)
				break;
				case "HISTORY":
					let history = _self.logicCola.getHistoryResults()				
					_self.elSocketTreballs.send([clientid,"",
							JSON.stringify({
								success: true,
								message: JSON.stringify(history)
							}), jobid])					
					break;
			default: //Always add cliendid and delimiter before to send [clientid,"", ....
				_self.elSocketTreballs.send([clientid,"",
					JSON.stringify({
						callError: {
							id: 500,
							callError: 500,
							message: "Request Not Found: " + peticio + " :: " + jobid
						}
					}), jobid]
				)
		}
	}
	checkMessagefromClient(_self, peticio, mfunc, jobid) {
		switch (peticio) {
			case "REGISTRY":
				let resgitry_json = _self.logicCola.registrarTrabajo(mfunc, jobid)
				if (resgitry_json) {
					console.log("[C] Funcion Registrada")
					_self.emit("REGISTRY", [resgitry_json, jobid]);
					_self.elSocket.send([
						JSON.stringify({
							success: true,
							message: "Resgitrado: " + resgitry_json.name_function
						}), jobid]
					)
				} else _self.elSocket.send([
					JSON.stringify({
						callError: {
							id: 404,
							success: false,
							message: `Resgitry Not Valid format`
						}
					}), jobid]
				)
				break;
			case "LAUNCH":
				let success = _self.logicCola.encargarTrabajo(jobid, mfunc,_self.clientID/**ID CLiente Unica, futuro Id Usuario */)
				if (success) {
					_self.elSocket.send([
						JSON.stringify({
							success: true,
							message: "Peticion Tarea Aceptada"
						}), jobid]
					)
					_self.emit("LAUNCH", [_self.clientID, "", JSON.stringify({ success: true, message: "Encargado" }), jobid]);
				} else if (success == false)
					_self.elSocket.send([
						JSON.stringify({
							callError: {
								id: 404,
								success: false,
								message: `Funcion ${launch_json.name_function} no resgistrada`,
								available:  _self.logicCola.getFuncionesRegistradas()
							}
						}), jobid])
				else _self.elSocket.send([
					JSON.stringify({
						callError: {
							id: 404,
							success: false,
							message: `Launh Not Valid format`
						}
					}), jobid])
				break;
				
			default:
				_self.elSocket.send([
					JSON.stringify({
						callError: {
							id: 500,
							callError: 500,
							message: "Request Not Found: " + peticio + " :: " + mfunc
						}
					}), jobid]
				)
		}
	}

	//Send Result [clientID,"",msg:JSON:string,jobid]
	sendResult(args) {
		this.elSocketTreballs.send(args)
	}
	// -----------------------------------------------------------------
	//  
	// -----------------------------------------------------------------
	cerrar() {
		this.elSocket.close()
		this.elSocketTreballs.close()
	}
} // class
class DispatcherWoker extends EventEmitter {

	/**
	 * 
	 * @param {*} logicCola 
	 * @param {*} tipoSocket 
	 * @param {*} miURLWorker 
	 * @param {*} answerInterval 
	 */
	constructor(logicCola, tipoSocket, miURLWorker, answerInterval = 4000) {
		super();
		this.logic = logicCola
		this.miURLWorker = miURLWorker;
		this.elSocketBackend = zmq.socket(tipoSocket)
		this.workers = [];
		//Workers sorted by slowest
		this.listWorkers = [];
		this.requestsPerWorker = [];
		this.ocupatsWorkers = [];
		this.answerInterval = answerInterval;
		this.map = {}; //{ "": { "clients": [], "peticiones": [] } };

		var self = this
		/**
		 * Misatges deguts a una tasca, l'ultim element sempre será jobid
		 */
		this.elSocketBackend.bindSync(miURLWorker, (err) => {

			if (err) {
				console.log("Error en bind: " + err)
			} else {
				console.log("[C] Listen workers on " + miURLWorker)
			}
		})
		
		this.elSocketBackend.on("message", function () {
			// Array of elements: [workerID,'', STATUS_WORKER, RESULT:opcional, jobid: opcional]
			// STATUS_WORKER: READY,DONE
			// -READY: msg from w = [workerID,'',STATUS_WORKER]
			// -DONE: msg from w = [workerID,'',STATUS_WORKER,RESULT:json,jobid]
			try {
				let args = Array.apply(null, arguments);
				var workerID = args[0].toString();
				var workerEstat = args[2].toString();
				self.checkMessagefromWorker(self, workerEstat, workerID, args)

			} catch (err) {
				self.elSocketBackend.send([workerID, "", "NotClient", "",
					JSON.stringify({
						callError: {
							id: 404,
							message: "Server Error: " + err
						}
					})]
				)
			}
		})
	}
	checkMessagefromWorker(_self, workerEstat, workerID, args) {

		switch (workerEstat) {
			case "READY":
				let CarregaWorker = args.pop();//Ultim del array
				if (CarregaWorker) CarregaWorker = CarregaWorker.toString();

				console.log(`[C] Worker Ready(${workerID}) status: ${workerEstat}`)
				//1. STORE WORKER LIKE A READY WORKER
				_self.readyWorker(workerID, CarregaWorker);
				//2. CHECK IF THERE IS NEW JOB TO WORKER
				if (!_self.encargoPendiente(workerID))//Algun client pendent?
						console.log(`[C] Sin encargos pendientes para ${workerID}`)
				_self.requestsPerWorker[workerID]++;
				
				break;
			case "DONE":
				let jobid = args.pop();
				let ResultatWorker = args.pop();
				
				if (_self.ocupatsWorkers[workerID]) {
					//0. Clear timeout of worker
					clearTimeout(_self.ocupatsWorkers[workerID].timeout);
					console.log(`\n[C] Worker(${workerID}) arrived on time [OK]`);
					//1. Set Job Solution on Queue
					_self.logic.setSolEncargo(jobid,ResultatWorker.toString())
					//2. Check if some client wait sol job
					let msg_array=_self.logic.existClientWaitJob(jobid)
					console.log(`[C] Some client waiting sol jobid(${jobid})?: `,{msg_array})
					//3. EMIT MESSAGE TO SOCKETRESULTS
					if(msg_array)
						_self.emit("DONE",msg_array); //EMIT MESSAGE ARRAY: [idclient,"",resultat:json,jobid]
					//4. CHECK IF THERE IS NEW JOB TO WORKER
					if (!_self.encargoPendiente(workerID))
						console.log(`[C] Sin encargos pendientes para ${workerID}`)
				} else {
					//0.b WORKER DELAY TO RESPONS
					console.log(`[C] ${workerID} arrived late, answer is rejected [TIMEOUT]\n`);
					return; 
				}
				_self.requestsPerWorker[workerID]++
				_self.map[workerID]["peticiones"].push(`${args[0]}:${args[1]}`)
				break;
			default:
				_self.elSocketBackend.send(
					JSON.stringify({
						callError: {
							id: 404,
							message: "Request amb format no válid"
						}
					})
				)
		}
	}


	addFunciones(json) {
		this.lasFunciones[json.name_function] = json;
	}

	encargoPendiente(workerID) {
		let encargo = this.logic.getEncargo();// return [OBJECT{name_function,code_function}, IDCLIENT, jobid]
		let m;
		if (encargo) {
			let IdCLiente=encargo[1]
			if (this.newClient(IdCLiente, workerID)) this.map[workerID]["clients"].push(IdCLiente);
			 m = [workerID, '',IdCLiente,'',JSON.stringify(encargo[0]),encargo[2]];
			this.sendToWorker(m, null);
			return true;
		} else {
			//No Order, No Order Client
			m = [workerID, '',null,''];
			this.sendToWorkerNotTask(m)
			return false
		};
	}

	sendToWorkerNotTask(msg){
		this.elSocketBackend.send(msg.concat("NOTJOB"));
	}
	//msg = "worker1", "", "client1", "", msg:JSON,jobid
	sendToWorker(msg, jobid) {
		let workerID = msg[0];
		//Point 1 is optional
		//1.Delete workers from current listWorkers availables
		delete this.listWorkers[workerID]

		this.ocupatsWorkers[workerID] = {}
		//2. A copy of the message sent to the worker is saved
		this.ocupatsWorkers[workerID].msg = msg.slice(2);
		this.ocupatsWorkers[workerID].jobid = jobid;
		
		//3. The timeout function is saved to set a maximum response time
		this.ocupatsWorkers[workerID].timeout = setTimeout(this.generateTimeoutHandler(this, workerID), this.answerInterval);

		console.log(`[C] Add Worker(${workerID}) to busyWorkers with msg(${this.ocupatsWorkers[workerID].msg} and waiting until the timeout(${this.answerInterval})`);

		//Send message to ---> Worker
		this.elSocketBackend.send(msg);
		/** The "timeout" og array[workerID] It will only have value if it arrives on time(answerInterval). Therefore,
		 *  if there is no value, the time for the worker to answer has been exceeded and the timout has occurred.**/
	}

	readyWorker(workerID/**workerID*/, Carrega) {
		if(!this.listWorkers[workerID]){
			console.log(`[c] Add readyWorker(${workerID}):(${Carrega})`)
			this.map[workerID] = { "clients": [], "peticiones": [] }
			this.listWorkers[workerID]=workerID+":"+Carrega
		}
	}
	newClient(c, myWorker) { for (let index in this.map[myWorker]["clients"]) if (this.map[myWorker]["clients"][index] == c) return false; return true; }

	generateTimeoutHandler(_self, workerID) {
		return function () {
			console.log('\n [C] worker (%s) timeout, worker deleted', workerID);
			//1. Grab the msg of the worker caigut
			var msg = _self.ocupatsWorkers[workerID].msg; //[clientID, delimiter, :Json, jobid]
			
			//We eliminated the dead worker from the island
			delete _self.ocupatsWorkers[workerID];
			// The task is put back on the list of pending orders
			console.log(`\n [C] Añadir de nuevo a la colaPedniente Job[${msg[3]}]`);
			_self.logic.encargarTrabajo(msg.pop(),msg.pop(),msg[0]); //jobid, mfunc,_self.clientID
			
		}
	}

	showStatistics() {
		var totalAmount = 0;
		console.log('\n\n **Current amount of requests served by each worker**');
		for (var i in this.requestsPerWorker) {
			console.log('   %s : %d requests', i, this.requestsPerWorker[i]);
			totalAmount += this.requestsPerWorker[i];
		}
		console.log('Requests already served (total): %d', totalAmount);
	}
	// -----------------------------------------------------------------
	//  
	// -----------------------------------------------------------------
	cerrar() {
		this.showStatistics();
		this.elSocketBackend.close()
	}

}

async function main() {
	
	var port_frontend = process.env.PORT_FRONTEND_COLA || 6001
	var port_worker = process.env.PORT_WORKER_COLA || 6002
	var port_frontendSolTask= process.env.PORT_FRONTENDSOL_COLA||6005

	var miURLFrontend = `tcp://0.0.0.0:${port_frontend}`
	var miURLFrontendTreballs = `tcp://0.0.0.0:${port_frontendSolTask}`

	var miURLWorker = `tcp://0.0.0.0:${port_worker}`

	console.log({ miURLWorker })
	// 
	// created a new Logica object
	// 
	var laLogica = await Logica.nueva()
	// 
	// created a new Logica object
	// to the functions of the logic according to
	// come in petitions
	// 
	var elDispatcher = new Dispatcher(laLogica, "rep", miURLFrontend, miURLFrontendTreballs)
	elDispatcher.on("REGISTRY", (obj) => {
		let funciones = laLogica.lasFunciones
		console.log("[C] Resgistrada tarea Actualmente:", { funciones });
	});
	elDispatcher.on("LAUNCH", (array) => {
		console.log("[C] Added encargo para su ejecucion")
	});

	var elDispatcherWorker = new DispatcherWoker( laLogica, "router", miURLWorker ) 
	elDispatcherWorker.on("DONE", (array) => {
		console.log("DONE",{array});
		elDispatcher.sendResult(array)
	});
	// 
	// capture control-c event
	// 
	process.on('SIGINT', function () {
		elDispatcherWorker.showStatistics();
		console.log("[C] Server Cola finished ")
		elDispatcherWorker.cerrar()
		elDispatcher.cerrar()
	})
}
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
main()