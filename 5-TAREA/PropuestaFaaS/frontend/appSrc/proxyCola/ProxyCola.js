// ---------------------------------------------------------------------
// ProxyCola.js 
// Job Queue Logic Proxy
// ---------------------------------------------------------------------

var zmq = require( "zeromq" )
var uuid = require('uuid');

// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
class ProxyCola {


	constructor( urlCola,urlColaTreballs, rechazar, resolver, myID=null ) {


		this.callbackRecepcion = null

		this.elSocket = zmq.socket( "req" )
		this.elSocketTreballs = zmq.socket( "req" )
		
		//ID only works with socket router, placed at the beginning of the array (args[0])
		this.elSocket.identity = myID != null ?myID:uuid.v1();
		this.mytaks=[];
		this.myrequest=[];
		var self = this

		console.log( `Init ProxyCola con ID socket [${this.elSocket.identity}]`)
		self.elSocket.on( "message", function( ) {
			var args =Array.apply(null, arguments);
			let current_jobid=args.pop().toString();
			let respuesta=args[0]
			console.log( `[F] ProxyCola: llega: ${respuesta} jobId: ${current_jobid}`)
			
			if(!self.mytaks[current_jobid]){
				console.log( " ProxyCola: horror: llega mensaje sin que haya callback para él ... " );
				return
			}

			let objRecibido = JSON.parse(respuesta)
			objRecibido.jobid=current_jobid
			console.log(objRecibido)
			self.mytaks[current_jobid](objRecibido.callError, objRecibido)
		})

		//MSG Format succesfully  [clientID,"",msg:JSON:string,jobid]
		self.elSocketTreballs.on( "message", function( ) {
			var args =Array.apply(null, arguments);
			let current_jobid=args.pop();
			let respuesta=args[0]
			console.log( "ProxyCola: llega: " + respuesta )
			
			let objRecibido = JSON.parse(respuesta)
			if(self.mytaks[current_jobid]){
				self.mytaks[current_jobid](objRecibido.callError, objRecibido)
				return
			}
			if(self.myrequest[current_jobid]){
				self.myrequest[current_jobid](objRecibido.callError, objRecibido)
			}

			console.log( " ProxyCola: horror: llega mensaje sin que haya callback para él ... " );
			return
		
		})

		this.elSocket.connect(urlCola)
		this.elSocketTreballs.identity = 'client_' + process.pid;
		this.elSocketTreballs.connect(urlColaTreballs)
		
		console.log(`[F] Connecting to ${urlCola}`)
		console.log(`[F] Connecting to Sol: ${urlColaTreballs}`)
		resolver( self )
	}

	registrarTrabajo(mJson){
		var self=this;
		return new Promise( (resolver, rechazar) => {
			let jobid=uuid.v1()
			self.mytaks[jobid] = function( callError, result ) {
				if( callError ) {
					rechazar( callError )
				} else {
					resolver( result )
				}
				self.mytaks[jobid] = null
			}
			self.elSocket.send( [ "REGISTRY", JSON.stringify(mJson),jobid ] )
		});
	}

	encargarTrabajo(mJson){
		let self=this;
		return new Promise( (resolver, rechazar) => {
			let jobid=uuid.v1()
			self.mytaks[jobid] = function( callError, result ) {
				if( callError ) {
					rechazar( callError )
				} else {
					resolver( result)
				}
				self.mytaks[jobid] = null
			}
			console.log(self.elSocketTreballs.identity )
			self.elSocket.send( [ "LAUNCH", JSON.stringify(mJson),jobid ] )
		});
	}

	getSolEncargo(jobid,wait=10000){
		console.log(`[C.F] Esperar solución job[${jobid}]`)
		
		let self=this;
		return new Promise( (resolver, rechazar) => {
			self.mytaks[jobid] = function( callError, result ) {
				clearTimeout(timeoutHandle);
				if( callError ) {
					rechazar( callError )
				} else {
					resolver( result )
				}
				self.mytaks[jobid] = null
			}
			let timeoutHandle = setTimeout(() => rechazar("TIMEOUT"), wait);
			//msg = [clientid,''] union [jobid, "WAIT_TASK", 5000]
			self.elSocketTreballs.send( [ jobid, "WAIT_TASK", wait ] )
		});
	}

	getHistory(){
		let self=this;
		return new Promise( (resolver, rechazar) => {
			let requestid=uuid.v4()
			self.myrequest[requestid] = function( callError, result ) {
				if( callError ) {
					rechazar( callError )
				} else {
					resolver( result )
				}
				self.myrequest[requestid] = null
			}
			//msg = [clientid,''] union [jobid, "WAIT_TASK", 5000]
			self.elSocketTreballs.send( [ requestid, "HISTORY", 5000 ] )
		});
	}
	// -----------------------------------------------------------------
	// 
	// -----------------------------------------------------------------
	cerrar() {
		this.elSocket.close()
		this.elSocketTreballs.close()
	}
								  
}
module.exports.nueva = function ( urlCola,urlColaTreballs ) {
	return new Promise( (resolver, rechazar) => {
		new ProxyCola(
			urlCola,urlColaTreballs,
			( err ) => {
				console.log( "[F] Logica.nueva() rechazar(): " + err )
				rechazar( err )
			},
			( logica ) => {
				console.log( "[F] Logica.nueva() resolver(): " + logica )
				resolver( logica ) 
			})
	} )
}