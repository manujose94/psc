// ---------------------------------------------------------------------
// Logica.js
// ---------------------------------------------------------------------
class Logica {

	constructor( proxyCola, proxyDB, rechazar, resolver  ) {

		this.proxyCola = proxyCola
		this.proxyDB = proxyDB
		resolver( this ) 
	}

	// -----------------------------------------------------------------
	// -----------------------------------------------------------------
	pruebaLogica( ) {
		return new Promise( (resolver, rechazar) => {
			resolver( " Hii from logic \n" )
		})
	}

								  
} // class

module.exports.nueva = function ( proxyCola, proxyDB ) {

	return new Promise( (resolver, rechazar) => {

		console.log( "Logica.nueva(): voy a new Logica ")
		// OJO: si new Logica va bien, devuelve un ptr a sí
		// mismo en el segubdo callback
		new Logica( proxyCola, proxyDB,
					( err ) => {
						console.log( "[ERROR] Logica.nueva() rechazar(): " + err )
						rechazar( err )
					},
					( logica ) => {
						//console.log( "Logica.nueva() resolver(): " + logica )
						resolver( logica ) 
					})
		} )
} // ()
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
