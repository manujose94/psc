// ---------------------------------------------------------------------
// mainserverREST.js
// ---------------------------------------------------------------------

// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
const express = require( 'express' ) 
const bodyParser = require( 'body-parser' )

const Logica = require( "../logic/Logica.js" )
const ProxyCola = require( "../proxyCola/ProxyCola.js" )

// ---------------------------------------------------------------------
// main()
// ---------------------------------------------------------------------
async function main() {
	var port_frontendSolTask= process.env.PORT_FRONTENDSOL_COLA||6005
	var port_frontend= process.env.PORT_FRONTEND_COLA||6001
	var hostname_cola= process.env.HOSTNAME_COLA|| "0.0.0.0"
	var urlCola = `tcp://${hostname_cola}:${port_frontend}`
	var urlColaTreballs = `tcp://${hostname_cola}:${port_frontendSolTask}`

	//ProxyCola
	var proxyCola = await ProxyCola.nueva( urlCola ,urlColaTreballs)
	//Logic
	var laLogica = await Logica.nueva( proxyCola, null ) 


	// Creating a server
	const  app = express();
	// Setting Base directory
	app.use(bodyParser.json());
	//CORS Middleware
	//app.use(morgan('dev'));
	app.use(function (req, res, next) {
		//Enabling CORS
		res.header("Access-Control-Allow-Origin", "*");
		res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,DELETE,POST,PUT");
		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, contentType,Content-Type, Accept, Authorization");
		next();
	});
	app.use(express.static('public'));
	app.use(bodyParser.urlencoded({extended: true}));
	// Global variables
	app.use((req, res, next) => {
		next();
	});
	// load REST rules
	var reglas = require( "./routes/rulesRest.js")
	reglas.load( app, proxyCola,laLogica,null )

	var server = app.listen( process.env.REST_PORT  || 8080, process.env.REST_HOST || '0.0.0.0', (err) => {
		if(err){
		console.error('Error starting  server', err);
		return;
		}
		let port = server.address().port;
		let address = server.address().address;
		console.log("[HOST URL]",`http://${address}:${port}/`);
	});
	
	if(process.env.NODE_ENV)
	try{
		let res = proxyCola.registrarTrabajo({name_function: "suma", code_function:"console.log(3+4)"})
		res = proxyCola.registrarTrabajo({name_function: "resta", code_function:"console.log(3-4)"})
		res = proxyCola.encargarTrabajo({name_function: "suma"})
		res = await proxyCola.encargarTrabajo({name_function: "suma"})
		console.log({res})
		let sol= await proxyCola.getSolEncargo(res.jobid)
		console.log({sol})
	}catch(e){
		console.log({e})
	}
	
	
	process.on('SIGINT', function() {
        server.close ()
		proxyCola.cerrar()
		console.log (" Finished ")
		process.exit();
	})
} // ()
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
main()
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------