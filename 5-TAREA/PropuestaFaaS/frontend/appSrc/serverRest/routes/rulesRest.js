const express = require('express');

module.exports.load = function( serverExpress,
    proxyCola,
    laLogica,
    proxyBD )
{
   
    serverExpress.post('/registrar', async (req, res) => {
    if (req.body ==null) res.send({ message: "Error Not found content function" , success: false})
    let {name_function,code_function}=req.body;
    let result
    try{
    result = await proxyCola.registrarTrabajo({"name_function":name_function, "code_function": code_function})//return json
    /**
     * launch contain a command line to launch a specific test via python
     */
    console.log({result})
    }catch(e){
        console.log({e})
        res.send({ message: "Error Trying to launch function "+name_function , success: false})
    }
    if(!result)
    res.send({ message: "Error Trying to launch function "+name_function , success: false})
    else
    res.send({ message: `${result.message}` , success: true});
});

serverExpress.post('/lanzar', async (req, res) => {
  if (req.body ==null) res.send({ message: "Error Not found content function" , success: false})
  let {name_function,code_function,wait}=req.body;
  let result
  try{
  result = await proxyCola.encargarTrabajo({"name_function":name_function})//return json
  if(wait && result.jobid) result= await proxyCola.getSolEncargo(result.jobid)//return json
  /**
   * launch contain a command line to launch a specific test via python
   */
  console.log({result})
  }catch(e){
      console.log({e})
      res.send({ message: "Error Trying to launch function "+name_function , success: false});return
  }
  if(!result)
  res.send({ message: "Error Trying to launch function "+name_function , success: false})
  else
  res.send(result);
});

serverExpress.get('/lanzar', async (req, res) => {
    if (req.param('name_function')==null) res.send({ message: "Error Not found name function" , success: false})
    let name_function=req.param('name_function');
    let wait=req.param('wait');
    let result
    try{
    result = await proxyCola.encargarTrabajo({"name_function":name_function})//return json
    if(wait && result.jobid) result= await proxyCola.getSolEncargo(result.jobid,wait)//return json
    /**
     * launch contain a command line to launch a specific test via python
     */
    console.log({result})
    }catch(e){
        console.log({e})
        res.send({ message: "Error Trying to launch function "+name_function , success: false})
        return
    }
    if(!result)
    res.send({ message: "Error Trying to launch function "+name_function , success: false})
    else
    res.send(result);
  });
serverExpress.get('/solution', async (req, res) => {
    let jobid = req.param('jobid');
    if (!jobid) res.send({ message: "Error Not found param jobid" , success: false})
    
    try{
    result = await proxyCola.getSolEncargo(jobid)//return json
    /**
     * launch contain a command line to launch a specific test via python
     */
    console.log({result})
    }catch(e){
        res.send({ message: "Error Trying to wait to get solution of job: "+jobid , success: false});return
    }
    if(!result)
    res.send({ message: "Error Trying to wait to get solution of job: "+jobid , success: false})
    else
    res.send(result);
  });
  
  serverExpress.get('/history', async (req, res) => {
    
    try{
    result = await proxyCola.getHistory()//return json
    /**
     * launch contain a command line to launch a specific test via python
     */
    console.log({result})
    }catch(e){
        res.send({ message: "Error Trying to get history: "+jobid , success: false})
    }
    if(!result)
    res.send({ message: "Error Trying to get history: "+jobid , success: false})
    else
    res.send(result);
  });
}
