// ---------------------------------------------------------------------
// ProxyCola.js 
// Proxy de la Logica de la cola de trabajo
// ---------------------------------------------------------------------

//var zmq = require( "zeromq" )
const Promise = require('bluebird')
const {AppDAO,ProjectFaas} = require('./dbDAO')

var self;
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
class ProxyDB {


	// -----------------------------------------------------------------
	// Txt ->
	//        constructor () ->
	// -----------------------------------------------------------------
	constructor(  ) {      
        self=this;
        this.init();
	} // ()

	init(){
        this.dao = new AppDAO('./database.sqlite3');
        this.projectFaas = new ProjectFaas(this.dao)
        this.projectFaas.createTable()
        .then(() => this.projectFaas.create())
        .then(() => this.projectFaas.getAll())
        .then((func) => {
            console.log({func})
          console.log(`\nRetreived function from database`)
          console.log(`funct id = ${func.id}`)
          console.log(`funct name = ${func.function_name}`)
        })
    }
								  
} 

// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
function main(){
    let p=new ProxyDB()
    
}
main()