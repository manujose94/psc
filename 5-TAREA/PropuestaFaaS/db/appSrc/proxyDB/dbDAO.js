
const sqlite3 = require('sqlite3')
const Promise = require('bluebird')
//https://stackabuse.com/a-sqlite-tutorial-with-node-js/
class AppDAO {
  constructor(db) {
    this.db = new sqlite3.Database(db, (err) => {
      if (err) {
        console.log('Could not connect to database', err)
      } else {
        console.log('Connected to database')
      }
    })
  }
  
  run(sql, params = []) {
    return new Promise((resolve, reject) => {
      this.db.run(sql, params, function (err) {
        if (err) {
          console.log('Error running sql ' + sql)
          console.log(err)
          reject(err)
        } else {
          console.log(sql)
          resolve({ id: this.lastID })
        }
      })
    })
  }

  get(sql, params = []) {
    return new Promise((resolve, reject) => {
      this.db.get(sql, params, (err, result) => {
        if (err) {
          console.log('Error running sql: ' + sql)
          console.log(err)
          reject(err)
        } else {
          resolve(result)
        }
      })
    })
  }

  all(sql, params = []) {
    return new Promise((resolve, reject) => {
      this.db.all(sql, params, (err, rows) => {
        if (err) {
          console.log('Error running sql: ' + sql)
          console.log(err)
          reject(err)
        } else {
          resolve(rows)
        }
      })
    })
  }
}

class ProjectFaas {
  constructor(dao) {
    this.dao = dao
  }

  createTable() {
    const sql = `
    CREATE TABLE IF NOT EXISTS functions (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      function_name varchar(50) DEFAULT NULL,
      function_code varchar(500) DEFAULT NULL,
      user_name varchar(25) DEFAULT NULL
    );`
    return this.dao.run(sql)
  }

  create() {
    return this.dao.run(
      `INSERT INTO functions (function_name,function_code,user_name) VALUES('TEST','console.log("MY TEST")',"tester");`);
  }


  getAll() {
    return this.dao.all(`SELECT * FROM functions`)
  }
}

module.exports = {AppDAO,ProjectFaas};
