const Logica = require( "./logica/Logica.js" )
const ProxyCola = require( "./proxyCola/ProxyCola.js" )
async function main() {
	var port_worker= process.env.PORT_FRONTEND_COLA||6002
    var hostname_cola= process.env.HOSTNAME_COLA|| "0.0.0.0"
	var urlCola = `tcp://${hostname_cola}:${port_worker}`
    process.on('SIGINT', function() {
		proxyCola.cerrar()
		process.exit();
	})
	var laLogica = await Logica.nueva()//Retries-fiulures 3

    var proxyCola = await ProxyCola.nueva( urlCola,laLogica )

	
}
main()