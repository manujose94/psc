// ---------------------------------------------------------------------
// Logica.js 
// Logic OFworker
// ---------------------------------------------------------------------
const exec = require('./commands');

class Logica {

	// -----------------------------------------------------------------
	//    constructor ()  -->
	// -----------------------------------------------------------------
	constructor(rechazar, resolver ) {
		this.limit_retries=3
		this.failures=0
		resolver( this ) 
	}

	// -----------------------------------------------------------------
	// -----------------------------------------------------------------
	
	maxLimitRetries(){
			if(this.failures==this.limit_retries){
				console.log("[L.W] Maximo de intentos de bucar encargo [EXIT]")
				
				return true
			}else{
				console.log(`[L.W] Cola sin Encargos pendientes Fallos: ${this.failures}`)
				
				return false
			}
	}

	addFailure(){
		this.failures++;
	}
	resetFailures(){
		this.failures=0
	}
	setRetries(Retries = 3){
		this.limit_retries=Retries
	}
	ferFunc (msg)  {
		return new Promise( async (resolver, rechazar) => {
		msg=JSON.parse(msg)
		let valid_msg=this.check_message(msg);
		console.log("[msg valid]: "+valid_msg)
		if(valid_msg){
		  let result;
		  try{
		   result = await exec.create_and_launch(valid_msg.name_function,valid_msg.code_function)       // compile an expression
		  console.log({result})
		  }catch(e){
			result= "Error"
		  }
		  resolver(result)
		}else{
			resolver({success:false, message:"Not valid message"});
		}

		} )
	  }

	 IsJsonString(str) {
		try {
			json =JSON.parse(str);
		} catch (e) {
			return false;
		}
		return json;
	}
	 check_message(msg){
	  let json_msg
	  if(msg instanceof Object)
	  	json_msg=msg;
	  else json_msg=this.IsJsonString(msg)
	  if(!json_msg)return false
	  if(json_msg.hasOwnProperty("code_function") && json_msg.hasOwnProperty("name_function"))
	  return json_msg
	  else return false
	}					  
} // class

module.exports.nueva = function () {
	return new Promise( (resolve, reject) => {
		new Logica(
			( err ) => {
				console.log( "Logica.nueva() REJEC(): " + err )
				reject( err )
			},
			( logic ) => {
				console.log("[W] ",{logic})
				resolve( logic ) 
			})
	} )
} // ()
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
