var zmq = require("zeromq");
var uuid = require('uuid');

// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
class ProxyCola {


	constructor(urlCola, rechazar, resolver, logic, retries, myID = null, LAG = 8000) {

		this.logic = logic;
		this.urlCola=urlCola
		this.isClose=false
		this.failures = 0
		if(retries)this.logic.setRetries(retries)

		this.elSocket = zmq.socket("req")
		//ID only works with socket router, placed at the beginning of the array (args[0])
		this.elSocket.identity = myID != null ? myID : uuid.v4();
		this.LAG=LAG;
		
		var self = this
		this.current_jobid = null
		console.log(`[W] Init ProxyCola with ID socket[${this.elSocket.identity}]`)
		//SOCKET REQ WORKER:> [MSG]:: "clientID", "", msg:JSON, JobId:Text	
		this.elSocket.on("message", async function (clientID, delimiter, msg, jobID) {
			clearTimeout(self.timeout)
			clearTimeout(self.FisrtresponseTimout)
			//1. Check Pending orders available at the Cola Server
			if(self.cola_withJOB(msg.toString())){
				self.current_jobid = jobID;
				console.log(`[W] Got New job ${jobID}, checking format ....`)
				//2. Check msg and make order or fucntion
				let mJson = self.checkFormat(msg.toString());
				if (mJson) {
					console.log(`[W] ProxyCola: arrive ${jobID}`, { mJson })
					if (mJson.hasOwnProperty("callError")) {
						console.log(`[W] Error ${mJson.callError.id} from Cola`)
						self.add_and_checkLimitFailures();
						return
					} else {
						self.logic.resetFailures()
						let result = await self.logic.ferFunc(msg);

						self.elSocket.send(["DONE", clientID, "", JSON.stringify(result), jobID])
						self.timeout = self.start_timeout(self,self.LAG)
					}
				} else { 
				//1.b If there are no outstanding tasks, add a fault
					console.log(`[W] Error message format invalid`); 
					self.add_and_checkLimitFailures();
				}
			}
		})

		this.elSocket.connect(urlCola)
		console.log(`[W] Connecting to ${urlCola}`)
		//First sending to server Cola
		this.FisrtresponseTimout=this.sendRequest(['READY', LAG]);
		console.log("[W] Firts messgae - Sending [READY, LAG] to [C]")
		this.timeout = this.start_timeout(this,LAG)
		resolver(self)


	} // ()

	cola_withJOB(msg){
		if(!msg)return false
		if(msg=="NOTJOB"){
			this.add_and_checkLimitFailures()
			return false
		}else return true
	}

	add_and_checkLimitFailures(){
		this.logic.addFailure()
		if(!this.logic.maxLimitRetries()){
			this.timeout = this.start_timeout(this,this.LAG)
		}else this.cerrar();
	}

	sendRequest(args,time=5000){
		this.elSocket.send(args);
		let _self=this;
		return setTimeout(() => { console.log(`[W] Cola doesn't response via ${_self.urlCola}`);_self.cerrar()},time);
	}
	start_timeout(_self,time) {
		
		if(_self.FisrtresponseTimout._idleTimeout==-1) //Firts response okey of COla Server
			return setTimeout(() => { if(!_self.isClose){console.log("[W] timout Sending [READY, LAG]"),_self.sendRequest(['READY', _self.LAG]);} }, time);
		else return setTimeout(() => { console.log("[W] Awaiting  response") }, time);
		
	}
	
	checkFormat(text) {
		if (typeof text !== "string") {
			return false;
		}
		try {
			let obj = JSON.parse(text);
			return obj;
		}catch (error) {
			console.log({ error })
			return false;
		}
	}
	

	cerrar() {	
		if(!this.isClose)
		this.elSocket.close()
		this.isClose=true
	}
	

}
module.exports.nueva = function (urlCola, logic) {
	return new Promise((resolve, reject) => {
		new ProxyCola(
			urlCola,
			(err) => {
				console.log("[W] Logica.nueva() rechazar(): ",err)
				reject(err)
			},
			(logic) => {
				resolve(logic)
			}, logic)
	})
} 