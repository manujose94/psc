// ---------------------------------------------------------------------
// mainServidor.js
// ---------------------------------------------------------------------
var zmq = require( "zeromq" )
const Logica = require( "../logica/Logica.js" )
var aux = require("./utils/auxfunctions.js");
const EventEmitter = require("events");

// ---------------------------------------------------------------------
// It is used to automatically call the
// of the logic, as requests come in on the socket
// zeromq
// ---------------------------------------------------------------------
class Dispatcher extends EventEmitter {

	// -----------------------------------------------------------------
	// 1.CallError are for errors in the queue logic itself.
	// For example: message received is not suitable or function not registered.
	//
	// 2. Launch function must be registered, if code_function exists in package
	// Allows to launch directly without registering
	// -----------------------------------------------------------------
	constructor( referenciasAFunciones, tipoSocket, urlSocket ) {
		super();
		this.lasFunciones = referenciasAFunciones
		this.elSocket = zmq.socket( tipoSocket )
		this.clientID="IDunicClient";
		var self = this

		// callback for receiving messages
		this.elSocket.on( "message", function() {
			try {
				// 1r element: Type of request REGISTRY or LAUNCH
				// 2n element:  Json amb {name_function: , code_function: }
				var args =Array.apply(null, arguments);
				var jobId=args.pop();
				console.log(jobId.toString())
				var peticio = args[0].toString()
				var mfunc = args[1].toString()

				console.log( `servidor: Arrives: ${peticio} : ${mfunc}`)

				self.checkMessagefromClient(self,peticio,mfunc,jobId)

			 } catch( err ) {
				self.elSocket.send(
					JSON.stringify( {
						callError: 500,
						message: "Server Error: " + err
					})
				)
			}
		}) 
	
		//Bind Socket
		self.elSocket.bind( urlSocket, ( err ) => {
			if( err ) {
				console.log( "Error en bind: " + err )
			} else {
				console.log( "escuchando en " + urlSocket )
			}
		})
	} // ()

	checkMessagefromClient(_self,peticio,mfunc,jobId){
		switch(peticio) {
			case "REGISTRY":
				let resgitry_json=aux.checkMessageRegistry(mfunc)
				if(resgitry_json){
					console.log("Mensaje para Registro")
					_self.lasFunciones[resgitry_json.name_function]=resgitry_json
					_self.emit("REGISTRY", [resgitry_json,jobId]);
					_self.elSocket.send([
						JSON.stringify({
							success: true,
							message: "Resgitrado: "+resgitry_json.name_function}),jobId]
					)
				}else _self.elSocket.send([
					JSON.stringify({ callError:{
						id: 404,
						success: false,
						message: `Resgitry Not Valid format`}}),jobId]
				)
			  break;
			case "LAUNCH":
				let launch_json=aux.checkMessageLaunch(mfunc)
				if(launch_json){
					if(_self.existFunciones(launch_json.name_function) || launch_json.code_function){ //Permitir sin registro lanzar funcion si hay código
						if(_self.lasFunciones[launch_json.name_function])
							launch_json["code_function"]=_self.lasFunciones[launch_json.name_function].code_function
						
					_self.emit("LAUNCH", [_self.clientID,"",JSON.stringify(launch_json),jobId]);
					}else 
					_self.elSocket.send([
						JSON.stringify({ callError:{
							id: 404,
							success: false,
							message: `Funcion ${launch_json.name_function} no resgistrada`}}),jobId])
					return
				}else _self.elSocket.send([
					JSON.stringify( { callError:{
						id: 404,
						success: false,
						message: `Launh Not Valid format`
						}
					}),jobId])
			  break;
			default:
				_self.elSocket.send([
					JSON.stringify( {callError:{
						id: 500,
						callError: 500,
						message: "Request Not Found: " + peticio+" :: "+mfunc}
					}),jobId]
				)
		  }
	}
	
	existFunciones(name_function){
		if(this.lasFunciones[name_function]) return true
		else false
	}

	sendResult(args){
		//Only one client always
		let jobID=args.pop()
		let result=args.pop()//String of Json
		//if there is more than one client this.elSocket.send(args) then args=[clientID,"",msg:JSON:string,jobID]
		this.elSocket.send([result,jobID])
	}
	
	cerrar( ) {
		this.elSocket.close()
	} // ()
} // class

class DispatcherWoker extends EventEmitter{

	constructor( referenciasAFunciones, tipoSocket, miURLWorker,answerInterval=4000 ) {
		super();
		this.miURLWorker=miURLWorker;
		this.lasFunciones = referenciasAFunciones
		this.elSocketBackend = zmq.socket( tipoSocket )
		this.workers=[];
		this.listWorkers_ordMenysLento=[];
		this.clientsEsperant=[];
		this.requestsPerWorker = []; 
		this.ocupatsWorkers = [];
		this.answerInterval = answerInterval; 
		this.map={}; //{ "": { "clients": [], "peticiones": [] } };

		var self = this
		this.elSocketBackend.bindSync(miURLWorker, ( err ) => {
			
			if( err ) {
				console.log( "Error en bind: " + err )
			} else {
				console.log( "escuchando en " + urlSocket )
			}
		})
		
		this.elSocketBackend.on( "message", function() {
			try {
				
				let args =Array.apply(null, arguments);
				//Maxim temps que el worker pot estar ocupat per fer una funcion al client
				
				var workerID = args[0].toString();
				var workerEstat=args[2].toString();//READY,DONE,OUT
				self.checkMessagefromWorker(self,workerEstat,workerID,args)
				
			 } catch( err ) {
				self.elSocketBackend.send([workerID,"","NotClient","",
					JSON.stringify( {callError:{
						id: 404,
						message: "Server Error: " + err
						}
					})]
				)
			}
		}) // elSocket.on
	} 
	checkMessagefromWorker(_self,workerEstat,workerID,args){

		switch(workerEstat) {
			case "READY":
				let CarregaWorker = args.pop();//Last element in array
				if(CarregaWorker)CarregaWorker=CarregaWorker.toString();

				console.log(`[C] Nou worker disponible(${workerID}) estat: ${workerEstat}`)
				if (!_self.processPendingClient(workerID))//Algun client pendent?
					_self.workers.push(workerID);
					_self.requestsPerWorker[workerID] = 0;
					_self.newWorker(workerID,CarregaWorker);
			  break;
			case "DONE":
				let jobId = args.pop();
				let ResultatWorker = args.pop();
				aux.insertWorkerLoad({ worker: workerID, load: 4000}, _self.listWorkers_ordMenysLento)
				if (_self.ocupatsWorkers[workerID]) {
					clearTimeout(_self.ocupatsWorkers[workerID].timeout);         
					console.log(`\n[C] Worker(${workerID}) Contesta a temps [OK]`);
					_self.emit("DONE", [args[3].toString(),"",ResultatWorker,jobId]); //[idclient,"",resultat:json]
				}else {
					console.log(`[C] ${workerID} arribat tart, contestacio rechazada [TIMEOUT]\n`);
					return; 
				}
				_self.requestsPerWorker[workerID]++
				_self.map[workerID]["peticiones"].push(`${args[0]}:${args[1]}`)
			  break;
			case "OUT":
				aux.deleteWorkerLoad(workerID)
				break
			default:
				_self.elSocketBackend.send(
					JSON.stringify( {
						callError:{
						id: 404,
						message: "Request amb format no válid"}
					})
				)
		  }
	}


	addFunciones(json){
		this.lasFunciones[json.name_function]=json;
	}
	

	processPendingClient(workerID) {
		if (this.clientsEsperant.length > 0) {
			var nextClient = this.clientsEsperant.shift();
			var m = [workerID, '', nextClient.id, ''].concat(nextClient.msg);
			m.concat(nextClient.jobId)
			this.sendToWorker(m, null);
			return true;
		} else return false;
	}


	despacharTarea(args) {
		//Hi han workers disponibles
		if (this.listWorkers_ordMenysLento.length > 0) {
			let myWorkerObj = aux.lowestPesos(this.listWorkers_ordMenysLento);
			let myWorker = myWorkerObj.worker;//ID WORKER
			if (this.newClient(args[0].toString(), myWorker)) this.map[myWorker]["clients"].push(args[0].toString());

			var m = [myWorker, ''].concat(args); // m = "worker1", "", "client1", "", msg:JSON,jobId
	
			console.log(`[C] Send to ${myWorker} with load(${myWorkerObj.load}) destined client, There are ${this.listWorkers_ordMenysLento.length} worker available`);
	
			//Send to available worker
			this.sendToWorker(m, null);
		} else{
			let jobId=args.pop()
			let mJson=args.slice(2);
			console.log(`[C] Client waits by to do: ${mJson}`)
			this.clientsEsperant.push({ id: args[0], msg: mJson, jobId: jobId }); //id = client1, mJson => msg
			console.log(`[C] Clients waiting response currently ${this.clientsEsperant}`)
		}
	}

	sendToWorker(msg) {
		let workerID = msg[0];
		
		// Initialise busyWorkers slot object.     
		this.ocupatsWorkers[workerID] = {}
		//The message sent to the worker is saved
		this.ocupatsWorkers[workerID].msg = msg.slice(2);
		//In timeout variable
		// the function is stored (generateTimeoutHandler(workerID))
		//	in the future it will have value when passing ->answerInterval
		this.ocupatsWorkers[workerID].timeout = setTimeout(this.generateTimeoutHandler(this,workerID), this.answerInterval);
	
		console.log(`[C] Add Worker(${workerID}) to busyWorkers with msg(${this.ocupatsWorkers[workerID].msg} and waiting until the timeout(${this.answerInterval})`);
	
		//Send message to ---> Worker
		this.elSocketBackend.send(msg);
	}

	newWorker(myWorker/**workerID*/,Carrega) {
		console.log(`[C] Add newWorker(${myWorker}):(${Carrega})`)
		this.map[myWorker] = { "clients": [], "peticiones": [] }
		aux.insertWorkerLoad({ worker: myWorker, load: Carrega}, this.listWorkers_ordMenysLento)
        aux.show(this.listWorkers_ordMenysLento);
	}
	newClient(c, myWorker) {for (index in this.map[myWorker]["clients"]) if (this.map[myWorker]["clients"][index] == c) return false; return true; }
	
	generateTimeoutHandler(_self,workerID) {
		return function () {
			 console.log('\n [C] worker (%s) timeout, worker deleted', workerID);
			//You get the message from the downed worker
			var msg = _self.ocupatsWorkers[workerID].msg;
			//Delete dead or too slow worker
			delete _self.ocupatsWorkers[workerID]; 
			// Resend new message
			_self.despacharTarea(msg); 
			console.log(`\n [C] Renviar petició del client : ${msg} a altre workert`);
		}
	}

	showStatistics() {
		var totalAmount = 0;
		console.log('\n\n **Current amount of requests served by each worker**');
		for (var i in this.requestsPerWorker) {
			console.log('   %s : %d requests', i, this.requestsPerWorker[i]);
			totalAmount += this.requestsPerWorker[i];
		}
		console.log('[C] Requests already served (total): %d', totalAmount);
	}
	// -----------------------------------------------------------------
	//  
	// -----------------------------------------------------------------
	cerrar( ) {
		this.elSocketBackend.close()
	} // ()

}
// ---------------------------------------------------------------------
// main()
// ---------------------------------------------------------------------
async function main() {
	// 
	// 
	var port_frontend= process.env.PORT_FRONTEND_COLA||6001
	var port_worker= process.env.PORT_WORKER_COLA||6002

	var miURLFrontend=`tcp://0.0.0.0:${port_frontend}`
	var miURLWorker=`tcp://0.0.0.0:${port_worker}`

	console.log({miURLWorker})
	// 
	// new Object Logica
	// 
	var laLogica = await Logica.nueva( )

	var referenciasAFunciones = []
	referenciasAFunciones["PRUEBA"] = laLogica.prueba

	// 
	// Creting Dispatcer, it will make use of the different logics involved.
	// 
	var elDispatcher = new Dispatcher( referenciasAFunciones, "rep", miURLFrontend )
	elDispatcher.on("REGISTRY", (obj) => {
		console.log("[C] REGISTRY",{referenciasAFunciones});
	});
	elDispatcher.on("LAUNCH", (array) => {
		elDispatcherWorker.despacharTarea(array)//args=["client1", "", msg:JSON,jobId]
	});	

	var elDispatcherWorker = new DispatcherWoker( referenciasAFunciones, "router", miURLWorker )
	elDispatcherWorker.on("DONE", (array) => {
		elDispatcher.sendResult(array)//args=["client1", "", msg:JSON,jobId]
	});	
	// 
	// CAPTURE control-c
	// 
	process.on('SIGINT', function() {
		elDispatcherWorker.showStatistics();
		console.log ("[C] Server Cola finished ")
		elDispatcherWorker.cerrar()
		elDispatcher.cerrar()
	})
} // ()
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
main()
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
