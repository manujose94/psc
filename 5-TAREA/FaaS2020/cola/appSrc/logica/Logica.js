
class Logica {

	// -----------------------------------------------------------------
	//    constructor ()  -->
	// -----------------------------------------------------------------
	constructor( refuse, resolve ) {

		console.log( "[LC] Logica.constructor() ")

		resolve( this ) 

	}

	prueba( ) {
		return "hola desde la logica del BO COLA\n" 
	}
							  
} // class

module.exports.nueva = function ( ) {
	return new Promise( (resolve, refuse) => {

		// OJO: if new Logica goes well, it returns a ptr to itself
		// same on the second callback
		new Logica( 
			( err ) => {
				console.log( "[CO] Logica.nueva() refuse(): " + err )
				refuse( err )
			},
			( logica ) => {
				console.log( "[CO] Logica.nueva() resolve(): " )
				console.log({logica})
				resolve( logica ) 
			})
	} )
} // ()
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
