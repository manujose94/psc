var zmq = require("zeromq");
var uuid = require('uuid');

// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
class ProxyCola {


	constructor( urlCola, rechazar, resolver,logica, myID=null, LAG=null ) {

        this.logica=logica;
		this.elSocket = zmq.socket("req")
		
		//ID unica solo funciona con socket router, colocado al principio del array (args[0])
        this.elSocket.identity = myID != null ?myID:uuid.v1();
		this.LAG=4000;

		
		var self = this

		console.log( `[W] Init ProxyCola con ID socket[${this.elSocket.identity}]`)
		//[MSG]:: "worker1", "", "client1", "", msg:JSON	
		this.elSocket.on( "message", async function( clientID, delimiter, msg,jobID ) {
			clearTimeout(self.timeout)
			self.current_jobid=jobID;
			let mJson=self.checkFormat(msg.toString());
			if(mJson){
				console.log( `[W] ProxyCola: llega encargo ${jobID}`,{mJson}  )
				if(mJson.hasOwnProperty("callError")){
					console.log( `[W] Error ${mJson.callError.id} desde Cola`)
					return
				}else{
				let result= await self.logica.fer_func(msg);

				self.elSocket.send(["DONE",clientID,"",JSON.stringify(result),jobID])
			}
		}else console.log(`[W] Error format de msg no correcte`)
			
		})

		console.log( "[W] ProxyCola: connect a " + urlCola)

		//Conn al Servidor COla
		self.elSocket.connect( urlCola )
		console.log(`[W] Connected to ${urlCola}`)
        //Primer enviament al Servidor de Cola
        self.elSocket.send(['READY',this.LAG]);

		resolver( self )

	
	} // ()

	checkFormat(text){
		if (typeof text!=="string"){
			return false;
		}
		try{
			let obj=JSON.parse(text);
			return obj;
		}
		catch (error){
			console.log({error})
			return false;
		}	
	}
	/**exit(){
		this.elSocket.send(['CERRAR']);
		this.cerrar();
	}**/

	cerrar() {
		this.elSocket.close()
	}
								  
}
module.exports.nueva = function ( urlCola, logica ) {

	return new Promise( (resolver, rechazar) => {
		new ProxyCola(
			urlCola,
			( err ) => {
				console.log( "[W] Logica.nueva() rechazar(): " + err )
				rechazar( err )
			},
			( logica ) => {
				console.log( "[W] Logica.nueva() resolver(): " + logica )
				resolver( logica ) 
			},logica)
	} )
} 