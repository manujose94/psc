// ---------------------------------------------------------------------
// Logica.js 
// Logica of worker
// ---------------------------------------------------------------------
const exec = require('./commands');

class Logica {

	constructor( refuse, resolve ) {

		console.log( "Logica.constructor() ")

		resolve( this ) 

	}

	// -----------------------------------------------------------------
	// -----------------------------------------------------------------
	

	fer_func (msg)  {
		console.log(`requested the msg: (${msg})`)
		return new Promise( async (resolve, refuse) => {
		msg=JSON.parse(msg)
		let valid_msg=this.check_message(msg);
		console.log("[msg valid]: "+valid_msg)
		if(valid_msg){
		  let result;
		  try{
		   result = await exec.create_and_launch(valid_msg.name_function,valid_msg.code_function)       // compile an expression
		  console.log({result})
		  }catch(e){
			result= "Error"
		  }
		  resolve(result)
		}else{
			resolve({success:false, message:"Not valid message"});
		}

		} )
	  }

	 IsJsonString(str) {
		try {
			json =JSON.parse(str);
		} catch (e) {
			return false;
		}
		return json;
	}
	 check_message(msg){
	  let json_msg
	  if(msg instanceof Object)
	  	json_msg=msg;
	  else json_msg=this.IsJsonString(msg)
	  if(!json_msg)return false
	  if(json_msg.hasOwnProperty("code_function") && json_msg.hasOwnProperty("name_function"))
	  return json_msg
	  else return false
	}


								  
} // class


module.exports.nueva = function ( ) {
	return new Promise( (resolve, refuse) => {
		new Logica( 
			( err ) => {
				refuse( err )
			},
			( logica ) => {
				console.log({logica})
				resolve( logica ) 
			})
	} )
} // ()

