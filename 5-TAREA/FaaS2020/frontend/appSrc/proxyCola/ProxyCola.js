// ---------------------------------------------------------------------
// ProxyCola.js 
// Work queue logic proxy
// ---------------------------------------------------------------------

var zmq = require( "zeromq" )
var uuid = require('uuid');

// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
class ProxyCola {


	constructor( urlCola, refuse, resolve, myID=null ) {


		this.callbackRecepcion = null

		this.elSocket = zmq.socket( "req" )
		
		//Unique ID only works with router socket, placed at the beginning of the array. (args[0])
		this.elSocket.identity = myID != null ?myID:uuid.v1();
		this.mytaks=[];

		var self = this

		console.log( `Init ProxyCola con ID socket [${this.elSocket.identity}]`)
		self.elSocket.on( "message", function( ) {
			var args =Array.apply(null, arguments);
			let current_jobid=args.pop().toString();
			let respuesta=args[0]
			console.log( `ProxyCola: llega msg ${respuesta} with JobID ${current_jobid} `)
			
			if(!self.mytaks[current_jobid]){
				console.log( "[C] ProxyCola: horror: llega mensaje sin callback para él ... " );
				return
			}

			let objRecibido = JSON.parse(respuesta)
			self.mytaks[current_jobid](objRecibido.callError, objRecibido)
		})
		self.elSocket.connect(urlCola)
		console.log(`[F] Connecting to ${urlCola}`)
		resolve( self )
	} // ()

	registrarTrabajo(mJson){
		var self=this;
		return new Promise( (resolve, refuse) => {
			let jobid=uuid.v1()
			self.mytaks[jobid] = function( callError, result ) {
				if( callError ) {
					refuse( callError )
				} else {
					resolve( result )
				}
				self.mytaks[jobid] = null
			}
			self.elSocket.send( [ "REGISTRY", JSON.stringify(mJson),jobid ] )
		});
	}

	encargarTrabajo(mJson){
		var self=this;
		return new Promise( (resolve, refuse) => {
			let jobid=uuid.v1()
			self.mytaks[jobid] = function( callError, result ) {
				if( callError ) {
					refuse( callError )
				} else {
					resolve( result )
				}
				self.mytaks[jobid] = null
			}
			self.elSocket.send( [ "LAUNCH", JSON.stringify(mJson),jobid ] )
		});
	}
	// -----------------------------------------------------------------
	// prueba() -> Txt
	// -----------------------------------------------------------------
	prueba( ) {
		console.log( "ProxyCola.prueba()  " )
		var self = this
		return new Promise( (resolve, refuse) => {
			resolve( "hola desde la logica de la cola\n" )
			self.callbackRecepcion = function( callError, result ) {
				if( callError ) {
					refuse( callError )
				} else {
					resolve( result )
				}
				self.callbackRecepcion = null
			}
			self.elSocket.send( [ "PRUEBA", null /*no hay argumentos*/ ] )
			self.elSocket.send( [ "REGISTRY", '{"name_function": "suma", "code_function":"console.log(3+4)"}' ] )
			self.elSocket.send( [ "LAUNCH", '{"name_function": "suma", "code_function":"console.log(3+4)"}' ] )
		}) // () 
	} // ()

	// -----------------------------------------------------------------
	// 
	// -----------------------------------------------------------------
	cerrar() {
		this.elSocket.close()
	}	  
}
module.exports.nueva = function ( urlCola ) {
	return new Promise( (resolve, refuse) => {
		new ProxyCola(
			urlCola,
			( err ) => {
				refuse( err )
			},
			( logica ) => {
				resolve( logica ) 
			})
	} )
} // ()
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
