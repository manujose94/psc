// ---------------------------------------------------------------------
// ReglasREST.js
// 
//  Según qué petición llega, llamar a una función de la lógica
// de un sitio u otro.
// 
//  Algún día: automatizarlo, porque se ve que es repetitivo el código
// ---------------------------------------------------------------------
module.exports.cargar = function( servidorExpress,
								  proxyCola,
								  proxyBD,
								  laLogica )
{
	// -----------------------------------------------------------------
    // GET /prueba
	// -----------------------------------------------------------------
    servidorExpress.get('/prueba/', function( peticion, respuesta ){
		console.log( " * GET /prueba " )
		respuesta.send( "¡Funciona!\n" )
	}) // get /prueba

	// -----------------------------------------------------------------
    // GET /pruebaLogica
	// -----------------------------------------------------------------
    servidorExpress.get('/pruebaLogica/', async function( peticion, respuesta ){
		console.log( " * GET /pruebaLogica " )
		var res = await laLogica.pruebaLogica()
		respuesta.send( res )
	}) // get /prueba

	// -----------------------------------------------------------------
    // GET /pruebaCola
	// -----------------------------------------------------------------
    servidorExpress.get('/pruebaCola/', async function( peticion, respuesta ){
		console.log( " * GET /pruebaCola " )
		var res = await proxyCola.prueba()
		respuesta.send( res )
	}) // get /prueba
	
} // ()
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------

