const express = require('express');

module.exports.load = function( serverExpress,
    proxyCola,
    laLogica,
    proxyBD )
{
   
    serverExpress.post('/registrar', async (req, res) => {
    if (req.body ==null) res.send({ message: "Error Not found content function" , success: false})
    let {name_function,code_function}=req.body;
    let result
    try{
    result = await proxyCola.registrarTrabajo({"name_function":name_function, "code_function": code_function})//return json
    /**
     * launch contain a command line to launch a specific test via python
     */
    console.log({result})
    }catch(e){
        console.log({e})
    }
    if(!result)
    res.send({ message: "Error Trying to launch function "+name_function , success: false})
    else
    res.send({ message: `${result.message}` , success: true});
});

serverExpress.post('/lanzar', async (req, res) => {
  if (req.body ==null) res.send({ message: "Error Not found content function" , success: false})
  let {name_function,code_function}=req.body;
  let result
  try{
  result = await proxyCola.encargarTrabajo({"name_function":name_function})//return json
  /**
   * launch contain a command line to launch a specific test via python
   */
  console.log({result})
  }catch(e){
      console.log({e})
  }
  if(!result)
  res.send({ message: "Error Trying to launch function "+name_function , success: false})
  else
  res.send(result);
});
}
