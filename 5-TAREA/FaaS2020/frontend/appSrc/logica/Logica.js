// ---------------------------------------------------------------------
// Logica.js
// ---------------------------------------------------------------------
class Logica {

	constructor( proxyCola, proxyDB, refuse, resolve  ) {

		this.proxyCola = proxyCola
		this.proxyDB = proxyDB
		resolve( this ) 
	}

	// -----------------------------------------------------------------
	// -----------------------------------------------------------------
	pruebaLogica( ) {
		return new Promise( (resolve, refuse) => {
			resolve( "Hii from Logic \n" )
		})
	}

								  
} // class

module.exports.nueva = function ( proxyCola, proxyDB ) {

	return new Promise( (resolve, refuse) => {
		new Logica( proxyCola, proxyDB,
					( err ) => {
						refuse( err )
					},
					( logica ) => {
						resolve( logica ) 
					})
		} )
} // ()
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------
