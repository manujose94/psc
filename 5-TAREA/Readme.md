# Faas

In this task, two Faas proposals are made with different implementations and logic.

### Objective

Develop an example that is capable of showing different ways of performing micro-services for the deployment of an Faas. 

In this case, in both proposals, through the use of a Frontend, requests are sent to Workers. These requests are functions in Javascript that are executed, resending their result.

### Tools used

- NodeJS
- ZeroMQ
- Express
- SQLite
- ShellScript
- Docker and Docker Compose



### Launch

A docker has been used for the deployment, but each component or asset of the project (frontend, tail and worker) can be launched individually.

With docker using **docker-compose**:

```shell
Faas2020/$ docker-compose build
Faas2020/$ docker-compose up -d
```

```shell
ProposalFaas/$ docker-compose build
ProposalFaas/$ docker-compose up -d
```

Launch individually using **npm**, we must place ourselves in the *appSrc* folder of each component that makes up each proposal, for example with Faas2020:

```shell
/FaaS2020/cola/appSrc$ npm start

/FaaS2020/worker/appSrc$ npm start

/FaaS2020/frontend/appSrc$ npm start
```



#### Launch Proposal2020 and Check

```shell
ProposalFaas/$ docker-compose up --build
```

> If you add the `--build` option, it is forced to build the images even when not needed

It is now possible to make http requests, for greater ease of use using Postman.

You can import the following template:

[PropuestasFaasPSC.postman_collection.json](./PropuestaFaaS/PropuestasFaasPSC.postman_collection.json)

## Projects

Launch individually medium **npm**, we must place ourselves in the *appSrc* folder of each component that makes up each proposal, for example with Faas2020:

### Faas2020

------



- Frontend
  - HTTP Rest server and handler with express
  - Send a request for an order (register or perform a function) to **Cola**
  - For each order or request a **UUID** is generated

- Cola
  - Queue servers using ZeroMQ
  - It contains a list of the recorded functions
  - Contains a list of prepared and employed workers
  - Contains a list of customers waiting for a request
  - Requests for customer functions are forwarded to available workers (listed in the queue)
  - Workers' responses are forwarded to customers by Cola itself.
  - Calls to functions (jobs) identified by **UID**

- Worker
  - Responsible for performing the function that arrives as a request of **Cola** complement.
  - Forwards a message with the solution and **UUID** of the request

Scheme of the general structure: 

<img src="./FaaS2020/img/4-tarea-logica-Faas.png" style="zoom:67%;" />

Note: Function names may not represent the name implemented in the code

### Proposal2020

------

An example project has been created following the design of the [Mi diseño Faas_V2.pdf](./Mi diseño FaaS _v2.pdf)

> ​	Note:  It remains to manage the call => Obtain result from a jobid that has never been launched

- Frontend
  - HTTP Rest server and handler with express
  - Send a request for an order (register or perform a function) to **Cola**
  - Send request for solution to an order sent to **Cola**
  - For each order or request, an **UID** is generated
- Cola
  - Queue server using ZeroMQ
  - Contains:
    - List of busy workers
    - List of the orders to be carried out by the workers 
    - List of functions registered by users
    - List of customers waiting for a solution of the order 
  - The answers of the workers are stored in a solution queue by **UID** of the assignment
  - Calls to functions (jobs) identified by **UIDD**
- Worker
  - Responsible for requesting if there is a pending order to be made to **Cola**.
  - In case of a nonstop order, with no pending orders in the queue, the queue is disconnected
  - Forward a message with the solution

Diagram of the general structure and zeromq <img src="./PropuestaFaaS/img/5-tarea-esquema de conexiones ZeroMQ-end.png" style="zoom:80%;" />



<img src="./PropuestaFaaS/img/5-tarea-logica-Faas.png" style="zoom: 67%;" />

Note: Function names may not represent the name implemented in the code.

#### Quick tests

In order to test the functioning, Postaman can be used by importing the following file: 

[PropuestasFaasPSC.postman_collection.json](./PropuestaFaaS/PropuestasFaasPSC.postman_collection.json)

This contains a series of REST requests already prepared to be launched locally.

#### Summary of how to launch with docker-compose

The following only builds the images, does not start the containers:

- `docker-compose build`

The following builds the images if the images **do not exist** and starts the containers:

- `docker-compose up`

If you add the `--build` option, it is forced to build the images even when not needed:

- `docker-compose up --build`

The following skips the image build process:

- `docker-compose up --no-build`

If the images aren't built beforehand, it fails.

The `--no-cache` option disables the Docker [build cache](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#leverage-build-cache) in the image creation process. This is used to cache each layer in the *Dockerfile* and to speed up the image creation reusing [layers](https://docs.docker.com/storage/storagedriver/#images-and-layers) (~ Dockerfile lines) previously built for other images that are identical.

------

### Pending tasks

- Add persistence via SQLite database
  
- Controlling resources
  
- Get more worker instances in case of bottlenecks