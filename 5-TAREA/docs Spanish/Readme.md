# Faas

En esta tarea se realizan dos propuestas Faas con diferentes implementaciones y Lógica. 

### Objetivo

Desarrollar un ejemplo que sea capaz de mostrar diferentes maneras de como realizar micro-servicios para el despliegue de un Faas. 

En este caso, en ambas propuestas, mediante el uso de un Frontend se envían peticiones a Workers. Estas peticiones son funciones en Javascript que son ejecutadas, reenviando su resultado.

### Herramientas utilizadas

- NodeJS
- ZeroMQ
- Express
- ShellScript
- Docker

### Lanzamiento

Se ha utilizado **docker** para el despliegue, pero se pueden lanzar individualmente cada componente o activo del proyecto (frontend,cola y worker).

Con docker utilizando **docker-compose**:

```shell
Faas2020/$ docker-compose build
Faas2020/$ docker-compose up -d
```

```shell
PropuestaFaas/$ docker-compose build
PropuestaFaas/$ docker-compose up -d
```

Lanzar individualmente utilizando **npm**, debemos situarnos en la carpeta *appSrc* de cada componente que conforma cada propuesta, por ejemplo con Faas2020:

```shell
/FaaS2020/cola/appSrc$ npm start

/FaaS2020/worker/appSrc$ npm start

/FaaS2020/frontend/appSrc$ npm start
```

**Test** de interacción cliente -servidor colas **PropuestaFaas**:

```shell
./PropuestaFaaS/frontend/appSrc$ npm run testserver  
```



## Proyectos

Existen dos proyectos que siguen la misma estructura y clases, variando solamente el nombre o número de funciones, así como la lógica llevada a cabo.

### Faas2020

- Frontend
  - Servidor y manejador HTTP Rest con express
  - Envía petición de un encargo (registrar o realizar una función) a **Cola**
  - Se genera por cada encargo o petición se genera una **UUID**

- Cola
  - Servidores de colas mediante ZeroMQ
  - Contiene una lista de las funciones registradas
  - Contiene una lista de los workers preparados y ocupados
  - Contiene una lista de los clientes en espera de una petición
  - Las peticiones a funciones de clientes son reenviadas a los workers disponible (Listados en la Cola)
  -  Las respuestas de los workers son reenviados a los clientes por la propia Cola.
  -  Llamadas a funciones (trabajos) identificados por **UUID**

- Worker
  - Encargado de realizar la función que le llega como petición de la cola.
  - Reenvía un mensaje con la solución y **UUID** de la petición

Esquema de la estructura general : 

<img src="../FaaS2020/img/4-tarea-logica-Faas.png" style="zoom:67%;" />

Nota: Los nombres de las funciones pueden no representar el nombre implementado en el código.

### Propuesta2020

Se ha creado un proyecto ejemplo siguiendo el diseño del documento [Mi diseño Faas_V2.pdf](./Mi diseño FaaS _v2.pdf)

> ​	Nota: Queda pendiere tratar la llamada=> Obtener resuktado de un trabajo(jobid) que nunca ha sido lanzado

- Frontend
  - Servidor y manejador HTTP Rest con express
  - Envía petición de un encargo (registrar o realizar una función) a **Cola**
  - Envía petición de la solución a un encargo enviado a **Cola**
  - Por cada encargo o petición se genera una **UUID**
- Cola
  - Servidor de colas mediante ZeroMQ
  - Contiene:
    - Lista de los workers ocupados
    - Lista de los encargos pendientes de ser realizados por los workers 
    - Lista de funciones registradas por los usuarios
    - Lista de los clientes en espera de una solución del encargo 
  - Las respuestas de los workers son almacenados en una cola de soluciones por **UUID** del encargo
  - Llamadas a funciones (trabajos) identificados por **UUID**
- Worker
  - Encargado de solicitar si hay un encargo pendiente a realizar a la **Cola**
  - En caso de solicitar n veces un encargo, sin encargos pendientes en la cola se desconecta
  - Reenvía un mensaje con la solución

Esquema de la estructura general y zeromq <img src="../PropuestaFaaS/img/5-tarea-esquema de conexiones ZeroMQ-end.png" style="zoom:80%;" />



<img src="../PropuestaFaaS/img/5-tarea-logica-Faas.png" style="zoom: 67%;" />

Nota: Los nombres de las funciones pueden no representar el nombre implementado en el código.