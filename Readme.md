# PSC

<img src="logo.png" style="zoom:80%;" />

## Cloud systems programming

In this UPV's assignment, a series of tasks have been carried out based on making structures for cloud systems.

The aim is that, starting with a program idea, we will be able to carry out this with a structure that is suitable for future deployment in a cloud system.

It has been used to perform the tasks:

- ZeroMQ Communication
- NodeJS language
- Different methods of solving some of the main problems of a cloud program such as:
  - Scalability
  - Fault-tolerant
  - Shared resources
  - Consistency

**Among all the exercises**, it is worth mentioning the [5-TAREA](5-TAREA/) , being the most complete of all