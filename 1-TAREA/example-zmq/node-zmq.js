var zmq = require('zeromq'),socketToRequest,subscriber_to_infoChannel;

var args = process.argv.slice(2);
var portChannel =args[1] ||'8059';
var myID = args[0] || 'None';
var nodos_channel = {}; //'key':'value' = 'idnodo':date
var idIntervalQuery;

/**
 * Falta detección de que el cliente se ha desconectado, por tanto quitar de la lista de nodos_channel
 */
function init_interval_to_show_infoChannel(){
   return setInterval(function () {
        console.log("Total Nodes subscribed:",JSON.stringify(nodos_channel))
    }, 8000);
}
/**
 * If Broker/Server close channel then close subscription to updates, channelm and Intervañ
 */
function exit_of_channel(){
    //close connections
    if(subscriber_to_infoChannel)
        subscriber_to_infoChannel.close()
    socketToRequest.close();
    //Clear interval to get information
    if(idIntervalQuery)
        clearInterval(idIntervalQuery);
    console.log("[END] Exit successfully")
    process.exit(0)
}
/**
 * Suscriber to updates about channel and timeout
 * @param {*} url_channel 
 */
function subs_get_nodes_of_channel(url){
    if(url){
        subscriber_to_infoChannel = zmq.socket('sub')
        subscriber_to_infoChannel.connect(url)
        subscriber_to_infoChannel.subscribe("")
        subscriber_to_infoChannel.on("message", function (reply) {
            check_message_publisher(reply)
        })
    }
    idIntervalQuery=init_interval_to_show_infoChannel()
}

function check_message_publisher(msg){
    try {
        let packet_from_server=JSON.parse(msg);
        getting_current_nodesChannel(packet_from_server)
    } catch (e) {
        if(msg && msg.toString()=='exit') exit_of_channel();
    }
}

//CHEK NEW NODES/MEMBERS IN CHANNEL
function getting_current_nodesChannel(received_nodos_channel){
    for(let idClient in received_nodos_channel) {
        if(!received_nodos_channel[idClient] && idClient!=myID)
            console.log("[NEW] Client in channel: "+received_nodos_channel)
    }
    nodos_channel=received_nodos_channel;
}

function send_myid(){
    myMsg="HELLO FROM NODE: "+myID
    //Enviament al broker , se li add "" a msg  
    socketToRequest.send([myID, myMsg]);//pasar el socketToRequest[req] el msg = [" ",myMsg]
}

/**
 * Firts connection to channel with timeout
 * @param {*} url_channel 
 */
// TIMEOUT
function timout_connect_channel (url_channel){
    return setTimeout(() => {
        clearTimeout(id);
        socketToRequest.close();
        console.log("[TIMEOUT] Channel not available ("+url_channel+")")
        process.exit(0)
    }, 8000);
}
// CONNECTION TO CHANNEL
function connect_to_channel(dir,port){
    brokerURL='tcp://'+dir+':'+port
    let id=timout_connect_channel(brokerURL);
    socketToRequest.connect(brokerURL);
    //CONNECTED THEN CLEAR TIMEOUT
    clearTimeout(id);
    //SEND MY ID
    send_myid()
}
function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
// WAIT THE REPLY FROM BROKER/SERVER (REP)
function wait_reply_of_broker(seconds){
   return new Promise(function (resolve, reject) {
        socketToRequest.on('message', function (msg) {//Respon el broker
            if(IsJsonString(msg)){
                let obj=JSON.parse(msg)
                if(obj.hasOwnProperty('url_to_get_infochannel'))
                    resolve(obj.url_to_get_infochannel)                  
            }else
                 resolve(msg)                  
        });
        setTimeout(function(){
          reject(new Error("timeout"));
        }, seconds*1000);
      })
}
//Get info about channel already connected
async function get_info_channel(){
    try {
        let url_about_nodes = await wait_reply_of_broker(5);
        console.log('[REPLY FROM BROKER] This is the url to get info channel:',url_about_nodes); //replyText
        subs_get_nodes_of_channel(url_about_nodes)
    } catch(err) {
        console.log("[ERROR] NOT POSSIBLE TO GET INFORMATION ABOUT THE CHANNEL ")
   }
}
//Process when client/node want to exit of channel
async function exit_node() {
    socketToRequest.send([myID,"exit"])
    let value= await wait_reply_of_broker(1)
    exit_of_channel()
}

// -----------------------MAIN-------------------------------
// --------------------------------------------------------
function main(port_channel,id){
    //SET MY ID
    if (id == 'None')
        myID="Node:"+(Math.random() * (100 - 1) + 1);
    //Create soclkey type REQ
    socketToRequest = zmq.socket('req');
    socketToRequest.identity = myID;
    
    //CONNECT TO CHANNEL AND SEND FIRST MESSAGE
    connect_to_channel('localhost',port_channel)
    //WAIT REPLY FROM BROKER/SERVER AND GET IFNO CHANNEL
    get_info_channel()
   //CLOSE VIA CTRL+C
   process.on('SIGINT',exit_node )
}

// --------------------------------------------------------
// --------------------------------------------------------
/**
 * LAUNCH MAIN
 */
main(portChannel,myID)

