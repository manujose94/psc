var zmq = require('zeromq'), 
socketToRespond,pub_info_channel;
var dateFormat = require('dateformat');
var args = process.argv.slice(2);
var Port =  8059;
var infoPort=8688
var nodos_channel = {}; //'idnode':'date'

function exist_client(key){
    if(nodos_channel.hasOwnProperty(key)) return true;
    else return false
}
// SEND MENSAJE TO CLIENT (REQ->REP)
function reply_to_node(){
    url_pub_info_channel='tcp://localhost:'+infoPort;
    socketToRespond.send(JSON.stringify({url_to_get_infochannel:url_pub_info_channel})); 
 }
//CHECK MENSAJE FROM CLIENT (REP) 
function is_new_Node(idnode){
    if(!exist_client(idnode)){
        nodos_channel[idnode]=dateFormat(new Date(), "yyyy-mm-dd h:MM:ss");
        reply_to_node();
        return true;
    }else return false;
}
//SEND INFO ABOUT CHANNEL (PUB)
function pub_to_all(){
  return  setInterval(function () {
        console.log("[CHANNEL INFO] Current Nodes :",JSON.stringify(nodos_channel))
        pub_info_channel.send(JSON.stringify(nodos_channel))
    }, 5000);
}

function start_to_pub_info_channel(){
    pub_info_channel = zmq.socket('pub');
    pub_info_channel.bind('tcp://*:'+infoPort, function (err) {
        if (err)
            console.log(err)
        else {
            console.log("[INFO] Pub info Channel on 8688...");
        }
    })
     /**
     *  START PUB TO SUBS
     *  Each 5s send message json about nodes on Channel currently
     */
    return pub_to_all()
}


//DISCONNECT AND CLOSE REQ AND PUB SOCKET
function disconnect_and_close_channel(time=100,idInterval=null){
    setTimeout(function () {
        console.log("[EXIT] Exit Broker")
        if(idInterval)
            clearInterval(idInterval);
        pub_info_channel.close();
        socketToRespond.close();
      }, time)
}

function check_msg_and_take_decision(packet){
    var message = packet.pop().toString();
    var idnode=packet[0].toString()
    if(!idnode)return false
    if(message=="exit"){ 
        node_want_desconnect_of_channel(idnode)
    } else
        if(is_new_Node(idnode))
            console.log("[NEW] New node added "+idnode)
    return true
}
//DESCONNECTION FROM A NODE OF CHANNEL
function node_want_desconnect_of_channel(idnode){
    delete nodos_channel[idnode];
    socketToRespond.send("Bye")
}
// -------------------MAIN----------------------------------
// --------------------------------------------------------
function main() {
    // Creatin a Socket (zeromq)
    // Type socket REP (reply)
    socketToRespond=zmq.socket('rep')
    // Bind -> Sokcet Available on specific Port
    socketToRespond.bindSync('tcp://*:' + Port, function(err) {
        if (err) {
            console.log(err)
        } else {
            console.log("Listening on "+Port+"...")
        }
    })

    /**
     *  SERVER REQ-REP
     *  Create a function when the new Node
     *  Send message once this has connected to a Channel
     */
    socketToRespond.on('message', function (/**arguments**/) { 
        
        var args = Array.apply(null, arguments);//args = "client1", "", msg, classID
        check_msg_and_take_decision(args)

    });

    /****
     *  Socket PUB
     *  Start to send info(curreny nodes) channel via PUB
     */
    idInterval=start_to_pub_info_channel();

   
    /* 
    * Manage event close Ctrl+C
    */
    process.on('SIGINT', function () {
        pub_info_channel.send("exit")
        disconnect_and_close_channel(100,idInterval)
    })
}

// --------------------------------------------------------
// --------------------------------------------------------
/**
 * LAUNCH MAIN
 */
main() 
