# Colleagues

This is a simple algorithm that aims to allow all nodes to get to know each other in a decentralised way.

It would be similar to how a whatsap group works. A P2P communication through zeroMQ connections.

### Steps

1. Start a first node (participant).
2. When a new node is started, it makes a 'hello' over the channel.
3. Now the two nodes know each other.
4. If a node already has a friend, it sends a hello along with its friend's information.

### Characteristics of each node

- A node has group information (all participants)
- It has a unique uuid or port assigned to it.
- If it detects that a node has sent it a 'hello', it communicates this to each participant in the group. 

### P2P channel 

![](req-rep-channel-zmq/schemas/canal_p2p_conexiones ZeroMQ.png)

### Source

[req-rep-channel-zmq](req-rep-channel-zmq/)