var zmq = require('zeromq');
var deepEqual = require('deep-equal');
var grupo = {}; //'key':'value'
var self;

class Colegas {

    constructor(participanteID){
        console.log(participanteID)
        self=this;
      this.participanteID = participanteID;
      this.grupo = {};
      this.grupo[participanteID]=participanteID;//'key':'value'
      this.emisor=zmq.socket('pub');
      this.emisor.bind('tcp://*:'+participanteID, function (err) {
        if (err)
            console.log(err)
        else {
            console.log("[INFO] Pub info Channel on ",participanteID);
        }
    })
    //this//.pub_to_all(this.grupo,this.emisor)
    }
  
     pub_to_all(grupo,emisor){
        return  setInterval(function () {
              console.log("[CHANNEL INFO] Grupo :",JSON.stringify(Object.keys(grupo)))
              emisor.send(JSON.stringify(Object.keys(grupo)))
          }, 3000);
      }
    print(){
      console.log('Grupo es :'+ this.grupo);
    }

    actualizar(g){
        console.log(deepEqual(Object.keys(g), Object.keys(this.grupo)))
        if(!deepEqual(Object.keys(g), Object.keys(this.grupo))){
            this.nuevosColegas(g)
            console.log("send")
            this.emisor.send(JSON.stringify(this.grupo))
        }else{
            console.log("[Colegas] g==grupo")
        }
    }

    nuevosColegas(g){
       
        let obj1Keys = Object.keys(g);
        let obj2Keys = Object.keys(this.grupo);

        for ( let i = 0; i < obj1Keys.length; i++ ) {
            for ( let j = 0; j < obj2Keys.length; j++ ) 

                if ( obj1Keys[i] !== obj2Keys[j]) {
                    
                    this.grupo[obj1Keys[i]]=null;
                    this.conectarConNuevos(obj1Keys[i])
                }
        }
    }
    conectarConNuevos(idnuevo){
        let receptor=zmq.socket('sub');
        receptor.connect('tcp://localhost:'+idnuevo)
        receptor.subscribe("")
        this.grupo[idnuevo]=receptor.on("message", function (g) {
            console.log("[REPLY] ESTE es mi grupo ",g)
            self.actualizar(JSON.parse(g))
        })
    }
}

  
module.exports=Colegas