let Colegas= require('./colegas-zmq.js');
var args = process.argv.slice(2);
var myID = args[0] || 'None';
var vencinoID= args[1];
var my_group = {};

// -----------------------MAIN-------------------------------
// --------------------------------------------------------
function main(myID){
    //SET MY ID
    if (myID == 'None')
        return 0;
    my_group[myID]=myID
    if(vencinoID)my_group[vencinoID]=vencinoID
    var colegis = new Colegas(myID);
    colegis.actualizar(my_group)
}

// --------------------------------------------------------
// --------------------------------------------------------
/**
 * LAUNCH MAIN
 */
main(myID)

