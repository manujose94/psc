var zmq = require('zeromq');
var deepEqual = require('deep-equal');
var self;

class Colegas {

    constructor(participanteID){
        console.log(participanteID)
        self=this;
      this.participanteID = participanteID;
      this.grupo = {};
      this.grupo[participanteID]=participanteID;//'ParticipanteID':'value'
      this.emisor=zmq.socket('rep');
      this.emisor.bind('tcp://*:'+participanteID, function (err) {
        if (err)
            console.log(err)
        else {
            console.log("Listening on",participanteID);
        }
    })
    //this//.pub_to_all(this.grupo,this.emisor)
    this.emisor.on('message', function (msg) { 
            console.log(msg)
        self.actualizar(JSON.parse(msg))

        });
    }
  
     pub_to_all(grupo,emisor){
        return  setInterval(function () {
              console.log("[CHANNEL INFO] Grupo :",JSON.stringify(Object.keys(grupo)))
              emisor.send(JSON.stringify(Object.keys(grupo)))
          }, 3000);
      }
    print(){
      console.log('Grupo es :'+ this.grupo);
    }

    actualizar(g){
        if(!deepEqual(g, Object.keys(this.grupo))){
            this.hay_nuevosColegas(g)
            console.log("send")
            this.emisor.send(JSON.stringify(this.grupo))
        }else{
            console.log("[Colegas] g==grupo")
        }
    }

    hay_nuevosColegas(g){
        let obj1Keys = g;
        let obj2Keys = Object.keys(this.grupo);
        for ( let i = 0; i < obj1Keys.length; i++ ) {
            for ( let j = 0; j < obj2Keys.length; j++ ) 
                if ( obj1Keys[i] !== obj2Keys[j]) {
                    this.grupo[obj1Keys[i]]=null;
                    this.conectarConNuevos(obj1Keys[i])
                }
        }
    }
    conectarConNuevos(idnuevo){
        let receptor= zmq.socket('req');
        receptor.connect('tcp://localhost:'+idnuevo)
        console.log(JSON.stringify(Object.keys(this.grupo)))
        receptor.send(JSON.stringify(Object.keys(this.grupo)))
        this.grupo[idnuevo]=receptor.on("message", function (g) {
            console.log("[REPLY] ESTE es mi grupo ",JSON.parse(g))
            self.actualizar(JSON.parse(g))
        })
        console.log(JSON.stringify(Object.keys(this.grupo)))
    }
}

  
module.exports=Colegas